#!/usr/bin/env python

import os
import logging
import stat
import argparse
import hashlib
import shutil
import tarfile
import multiprocessing

HASH_LENGTH = 8

def hash_file(filename) -> str:
    with open(filename, "rb", buffering=0) as f:
        return hash_fileobj(f)

def hash_fileobj(f) -> str:
    h = hashlib.sha256()
    for b in iter(lambda: f.read(128 * 1024), b""):
        h.update(b)
    return h.hexdigest()

def main():
    logging.basicConfig(format="%(message)s")
    logger = logging.getLogger("copy")
    logger.setLevel(logging.WARNING)

    args = argparse.ArgumentParser(description="...",
                                   formatter_class=argparse.RawTextHelpFormatter)
    args.add_argument("from_path", metavar="from", help="from")
    args.add_argument("to_path", metavar="to", help="to")

    args = args.parse_args()

    from_path = os.path.normpath(args.from_path)
    to_path = os.path.normpath(args.to_path)

    cpu_count = multiprocessing.cpu_count()

    processes = [multiprocessing.Process(target=handle_tar, args=(logger, from_path, to_path, i, cpu_count)) for i in range(cpu_count)]

    for process in processes:
        process.start()

    for process in processes:
        process.join()


def handle_tar(logger, from_path: str, to_path: str, process_number: int, cpu_count: int):
    tar = tarfile.open(from_path, "r")

    for i, member in enumerate(tar.getmembers()):
        if i % cpu_count != process_number:
            continue
        if member.isfile() or member.islnk():
            f = tar.extractfile(member)
            file_hash = hash_fileobj(f)
            filename = file_hash[0:HASH_LENGTH] + ".bin"
            to_abs = os.path.join(to_path, filename)

            if os.path.exists(to_abs):
                logger.info("Exists, skipped {} ({})".format(to_abs, member.name))
            else:
                logger.info("Extracted {} ({})".format(to_abs, member.name))
                to_file = open(to_abs, "wb")
                f.seek(0)
                shutil.copyfileobj(f, to_file)


if __name__ == "__main__":
    main()
