#!/usr/bin/env sh
python /mininet/miniedit.py >/dev/ttyS0  &
MININET_PID=$!
while sleep 1; do test -f "save_watch" && rm save_watch && kill -s USR1 $MININET_PID; done  &
USR1_PID=$!
while sleep 1; do test -f "restore_watch" && rm restore_watch && kill -s USR2 $MININET_PID; done &
USR2_PID=$!
while sleep 1; do echo -n "*" >/dev/ttyS0; done
ECHO_PID=$!


wait $MININET_PID
wait $USR1_PID
wait $USR2_PID
wait $ECHO_PID